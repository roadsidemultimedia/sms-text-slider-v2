<?php
/*
Plugin Name: V2 Basic Text Slider V2
Plugin URI: http://www.roadsidemultimedia.com
Description: Responsive Text Slider with bullet nav, and mobile touch nav 
Author: Curtis Grant
PageLines: true
Version: 1.0.4
Section: true
Class Name: RSBasicTextSliderV2
Filter: slider, gallery, roadside
Loading: active
Text Domain: dms-sms
Bitbucket Plugin URI: https://bitbucket.org/roadsidemultimedia/sms-image-revealer
Bitbucket Branch: master
*/
if( ! class_exists( 'PageLinesSectionFactory' ) )
  return;
global $rsbasicv2_slider_num;
$rsbasicv2_slider_num = 0;

class RSBasicTextSliderV2 extends PageLinesSection {

  function section_styles(){
    wp_enqueue_script( 'lightSlider-js', $this->base_url.'/js/jquery.lightSlider.js', array( 'jquery' ), pl_get_cache_key(), true );
    wp_enqueue_style( 'lightSlider-css', $this->base_url.'/css/lightSlider.css');
  }

  function section_opts(){


    $options = array();    
    
    $options[] = 
      array(
      'key'   => 'array',
        'type'    => 'accordion', 
      'col'   => 1,
      'title' => 'Slides',
      'opts_cnt'  => 3,
      'title'   => __('Text Slider Slides', 'pagelines'), 
      'opts'  => array(
        array(
          'key'   => 'rsbtext',
          'label'   => __( 'text in slider', 'pagelines' ),
          'type'    => 'textarea',
        ),


      )
      );
       $options[] =
        array(
        'type'  => 'multi',
        'key' => 'rs_nav_nav',
        'title' => 'Slider Settings',
        'col' => 2,
        'opts'  => array(
            array(
              'type'      => 'select',
              'key'     => 'rsbasicv2_slider_align',
              'label'     => 'Slide Alignment',
              'col' => 2,
              'opts'      => array(
                'sleft'   => array('name' => 'Left'),
                'scenter'  => array('name' => 'Center'),
                'sright'   => array('name' => 'Right'),            
              )
            ),
            array(
              'type'      => 'select',
              'key'     => 'rsbasicv2_slider_auto',
              'label'     => 'Auto Slide?',
              'col' => 2,
              'opts'      => array(
                'true'   => array('name' => 'Yes'),
                'false'  => array('name' => 'No'),            
              )
            ),
            array(
              'type'      => 'select',
              'key'     => 'rsbasicv2_slider_mode',
              'label'     => 'Slide Mode',
              'col' => 2,
              'opts'      => array(
                'slide'   => array('name' => 'Slide'),
                'fade'  => array('name' => 'Fade'),            
              )
            ),
            array(
              'type'      => 'text',
              'key'     => 'rsbasicv2_slider_speed',
              'label'     => 'Speed in Milliseconds Default 3000',
              'col' => 2,
            ),
        )
    );
  
    
    return $options;

  }


  function get_content( $array ){
    
    
    $out = '';
    
    if( is_array( $array ) ){
      foreach( $array as $key => $item ){
        $rsbtext = pl_array_get( 'rsbtext', $item ); 
        if( $rsbtext ){
         
          $out .= sprintf(
            '<li><p>%s</p></li>', 
            $rsbtext
          );
        }

      }
    }
    
    
    return $out;
  }

   function section_template( ) { 
    
    $array = $this->opt('array');
    $v2slidemode = ( $this->opt('rsbasicv2_slider_mode') ) ? $this->opt('rsbasicv2_slider_mode') : "slide";
    $v2slideauto = ( $this->opt('rsbasicv2_slider_auto') ) ? $this->opt('rsbasicv2_slider_auto') : "true";
    $v2slidealign = ( $this->opt('rsbasicv2_slider_auto') ) ? $this->opt('rsbasicv2_slider_align') : "left";
    $v2slidespeed = ( $this->opt('rsbasicv2_slider_speed') ) ? $this->opt('rsbasicv2_slider_speed') : "3000";

  global $rsbasicv2_slider_num;
   if(!$rsbasicv2_slider_num) {
     $rsbasicv2_slider_num = 1;
   }
   ?>
   <div class="tsv2holder <?php echo $v2slidealign; ?>">
  <ul id="v2textslider<?php echo $rsbasicv2_slider_num; ?>">
  
    <?php

    $out = $this->get_content( $array ); 

    echo $out;

    ?>
  </ul>
</div>
  <script type="text/javascript">
    jQuery(document).ready(function($) {
      $("#v2textslider<?php echo $rsbasicv2_slider_num; ?>").lightSlider({
        minSlide:1,
        maxSlide:1,
        auto: <?php echo $v2slideauto; ?>,
        pause: <?php echo $v2slidespeed; ?>,
        mode: '<?php echo $v2slidemode; ?>',
      });
    });
  </script>

<?php 
$rsbasicv2_slider_num++;
}


}